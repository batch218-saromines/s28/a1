//Create a new db called hotel

//Insert a isngle room insertOne

/*details

name - single
accommodates 2
price 1000
description A simple room with all the basic necessities


Insert multiple rooms (insertMany)


name - double
accommodates - 4
price 4000
des - a room with a queen sized bed perfect for a simple getaway
rooms_available - 15
isAvailable - false




*/


db.rooms.insertOne(
{
	name: 'single',
	accommodates: 2,
	price: 1000,
	description: 'A simple room with all the basic necessities',
	rooms_available: 5,
	isAvailable: false
}
);

db.rooms.insertMany([
{
	name: 'double',
	accommodates: 3,
	price: 2000,
	description: 'A room fit for a small family going on a vacation',
	rooms_available: 5,
	isAvailable: false
},
{
	name: 'queen',
	accommodates: 4,
	price: 4000,
	description: 'A room with a queen sized bed perfect for a simple getaway',
	rooms_available: 15,
	isAvailable: false
}
]);


db.rooms.find();
db.rooms.find({name: "double"});

db.rooms.updateOne(
	{ name : "queen"},
	{
	$set : {
		rooms_available : 0
	}
	}
);

db.rooms.deleteMany({
	rooms_available : 0
});
